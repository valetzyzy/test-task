<?php

require_once 'DbConnection.php';

class DB {

    private $pdo;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $this->pdo = DbConnection::getInstance()->getPdo();
    }

    /**
     * @param array $data
     * @return string
     */
    public function findMerchantByIdOrNew($data = [])
    {
        $statement = $this->pdo->prepare('SELECT * FROM merchants where mid = :mid');
        $statement->bindValue(':mid', $data['id']);
        $statement->execute();
        if ($merchant = $statement->fetch()) {
            return $merchant['mid'];
        } else {
            $statement = $this->pdo->prepare('INSERT INTO merchants(mid, dba) VALUES(:mid, :dba)');
            $statement->bindValue(':mid', $data['id']);
            $statement->bindValue(':dba', $data['name']);
            $statement->execute();

            return $this->pdo->lastInsertId();
        }
    }

    /**
     * @param array $data
     * @return string
     */
    public function findBatchByIdOrNew($data = [])
    {
        $statement = $this->pdo->prepare('SELECT * FROM batches where batch_date = :date and batch_ref_num = :ref');
        $statement->bindValue(':date', $data['date']);
        $statement->bindValue(':ref', $data['ref']);
        $statement->execute();
        if ($batch = $statement->fetch()) {
            return $batch['id'];
        } else {
            $statement = $this->pdo->prepare('INSERT INTO batches(batch_date, batch_ref_num) VALUES(:date, :ref)');
            $statement->bindValue(':date', $data['date']);
            $statement->bindValue(':ref', $data['ref']);
            $statement->execute();

            return $this->pdo->lastInsertId();
        }
    }




    private function find()
    {

    }

    /**
     * Insert data to db with transaction
     *
     * @param array $data
     * @return bool
     */
    public function pdoInsert(array $data)
    {
        $rowsSQL = [];
        $toBind = [];
        $columnNames = array_keys($data[0]);
        foreach($data as $arrayIndex => $row){
            $params = array();
            foreach($row as $columnName => $columnValue){
                $param = ":" . $columnName . $arrayIndex;
                $params[] = $param;
                $toBind[$param] = $columnValue;
            }
            $rowsSQL[] = "(" . implode(", ", $params) . ")";
        }

        $sql = "INSERT INTO `transactions` (" . implode(", ", $columnNames) . ") VALUES " . implode(", ", $rowsSQL);

        $pdoStatement = $this->pdo->prepare($sql);

        $this->pdo->beginTransaction();

        foreach($toBind as $param => $val){
            $pdoStatement->bindValue($param, $val);
        }

        try {
            $pdoStatement->execute();
        } catch (Exception $e) {
            $this->pdo->rollBack();
            self::log($e->getMessage());
        }

        $this->pdo->commit();
        return true;
    }


}