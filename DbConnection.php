<?php


class DbConnection
{
    private static $instance = null;
    private $pdo;

    private $dbUsername = 'root';
    private $dbPass = '1';
    private $db = 'test';
    private $host = '0.0.0.0';


    private function __construct()
    {
        $this->pdo = new PDO("mysql:host={$this->host}; dbname={$this->db}", $this->dbUsername, $this->dbPass,
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    public static function getInstance()
    {
        if (self::$instance == null){
            self::$instance = new DbConnection();
        }

        return self::$instance;
    }

    public function getPdo()
    {
        return $this->pdo;
    }

}