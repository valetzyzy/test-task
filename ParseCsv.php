<?php

require_once 'ReportFields.php';
require_once 'DbConnection.php';
require_once 'DB.php';


class ParseCsv
{
    private $file;
    private $mapping = [];
    private $db;

    /**
     * ParseCsv constructor.
     *
     * @param string $filePath
     * @param $mapping
     * @throws Exception
     */
    public function __construct(string $filePath, array $mapping)
    {
        if (!file_exists($filePath)) throw new Exception('Cannot open file');
        if (!$mapping) throw new Exception('Mapping should be present');

        $this->file = $filePath;
        $this->mapping = $mapping;
        $this->db = new DB();
    }

    /**
     * Read CSV lines
     *
     * @return Generator
     */
    public function getLines()
    {
        $f = fopen($this->file, 'r');
        try {
            while ($line = fgets($f)) {
                yield $line;
            }
        } finally {
            fclose($f);
        }
    }

    /**
     * Validate if all header are present
     *
     * @param string $line
     * @return array
     */
    public function validateHeaders(string $line) : array
    {
        $fileHeaders = str_getcsv($line);
        $errors = [];
        $headers = [];
        foreach ($fileHeaders as $line) {
            if (array_search($line, array_values($this->mapping)) !== false) {
                $headers[] = $line;
            } else {
                $errors[] = "Header $line doesn't exist";
            }
        }

        if (!empty($errors)) $headers['errors'] = $errors;

        return $headers;
    }

    /**
     * @param $lines
     * @param $chunkSize
     */
    public function saveToDb($lines, $chunkSize)
    {
        if ($chunkSize > 1) {
            $chunks = array_chunk($lines, $chunkSize);
            self::log('Total chunks: ' . count($chunks), 'Info');
            foreach ($chunks as $key => $chunk) {
                $lines = [];
                $batches = [];
                $merchantIds = [];
                $dataToInsert = [];
                foreach ($chunk as $line) {
                    $dbLine = $this->mapCsvToDb($line);
                    $merchantIds[] = [
                        'id' => $dbLine[ReportFields::MERCHANT_ID],
                        'name' => $dbLine[ReportFields::MERCHANT_NAME]
                    ];
                    $batches[] = [
                        'date' => $dbLine[ReportFields::BATCH_DATE],
                        'ref' => $dbLine[ReportFields::BATCH_REF_NUM]
                    ];
                }

                //save merchants
                //unique
                $merchantIds = array_map("unserialize", array_unique(array_map("serialize", $merchantIds)));
                foreach ($merchantIds as $merchantData) {
                    $this->db->findMerchantByIdOrNew($merchantData);
                }

                //save batches
                //unique
                $batches = array_map("unserialize", array_unique(array_map("serialize", $batches)));
                foreach ($batches as $batch) {
                    $this->db->findBatchByIdOrNew($batch);
                }

                self::log('Current chunk: ' . $key, 'Info');
                $this->db->pdoInsert($this->prepareTransactions($chunk));
            }
        }
    }



    /**
     * Map to db fields
     *
     * @param array $line
     * @return array
     */
    private function mapCsvToDb(array $line) : array
    {
        return [
            ReportFields::MERCHANT_ID => $line[$this->mapping[ReportFields::MERCHANT_ID]],
            ReportFields::MERCHANT_NAME => $line[$this->mapping[ReportFields::MERCHANT_NAME]],
            ReportFields::BATCH_DATE => $line[$this->mapping[ReportFields::BATCH_DATE]],
            ReportFields::BATCH_REF_NUM => $line[$this->mapping[ReportFields::BATCH_REF_NUM]],
            ReportFields::TRANSACTION_DATE => $line[$this->mapping[ReportFields::TRANSACTION_DATE]],
            ReportFields::TRANSACTION_TYPE => $line[$this->mapping[ReportFields::TRANSACTION_TYPE]],
            ReportFields::TRANSACTION_CARD_TYPE => $line[$this->mapping[ReportFields::TRANSACTION_CARD_TYPE]],
            ReportFields::TRANSACTION_CARD_NUMBER => $line[$this->mapping[ReportFields::TRANSACTION_CARD_NUMBER]],
            ReportFields::TRANSACTOIN_AMOUNT => $line[$this->mapping[ReportFields::TRANSACTOIN_AMOUNT]],
        ];
    }

    /**
     * Replace array keys with new keys
     *
     * @param array $array
     * @param array $keys
     * @param bool $filter
     * @return array
     */
    public static function arrayReplaceKeys(array $array, array $keys, $filter = false) : array
    {
        $newArray = [];
        foreach($array as $key => $value) {
            if(isset($keys[$key])) {
                $newArray[$keys[$key]] = $value;
            } elseif(!$filter) {
                $newArray[$key] = $value;
            }
        }

        return $newArray;
    }

    /**
     * Log something to console
     *
     * @param $message
     * @param string $type
     */
    public static function log($message, $type = 'Error')
    {
        echo "$type: $message" . PHP_EOL;

        if ($type == 'Error') exit;
    }

    /**
     *
     * @todo refactor this method, dont really like it
     * @param array $lines
     * @return array
     */
    private function prepareTransactions(array $lines)
    {
        if (empty($lines)) return [];

        $dataToInsert = [];
        foreach ($lines as $line) {
            $dbLine = $this->mapCsvToDb($line);
            $batchId = $this->db->findBatchByIdOrNew(['date' => $dbLine[ReportFields::BATCH_DATE], 'ref' => $dbLine[ReportFields::BATCH_REF_NUM]]);
            $dbLine['batch_id'] = $batchId;
            unset($dbLine[ReportFields::BATCH_REF_NUM]);
            unset($dbLine[ReportFields::BATCH_DATE]);
            unset($dbLine[ReportFields::MERCHANT_NAME]);

            $dataToInsert[] = $dbLine;
        }

        return $dataToInsert;
    }
}