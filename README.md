### Install

- setup db connection in `DbConnection.php` file
- create db table from `SQL/db.sql` file
- setup csv file path in `index.php`
- run it `php index.php`

All db selects is on `SQL/selects.sql`