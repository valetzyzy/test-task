CREATE TABLE batches
(
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  batch_date date,
  batch_ref_num varchar(24)
)

create table merchants
(
    mid bigint PRIMARY KEY NOT NULL AUTO_INCREMENT,
    dba varchar(100)
)

CREATE TABLE transactions
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    mid bigint not null,
    batch_id int not null,
    trans_date date,
    trans_type varchar(20),
    trans_card_type varchar(2),
    `trans_card_num` varchar(20),
    trans_amount float
);

ALTER TABLE transactions
ADD CONSTRAINT transactions_merchants_id_fk
FOREIGN KEY (mid) REFERENCES merchants (mid) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE transactions
ADD CONSTRAINT transactions_batches_id_fk
FOREIGN KEY (batch_id) REFERENCES batches (id) ON DELETE CASCADE ON UPDATE CASCADE;