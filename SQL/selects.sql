--  display all transactions for a batch (merchant + date + ref num)
--  *      date, type, card_type, card_number, amount
select b.id as batchId, t.trans_date, t.trans_type, t.trans_card_type, t.trans_card_num, t.trans_amount
from batches b
JOIN transactions t on b.id = t.batch_id;

-- display stats for a batch
select b.id as batchId, TRUNCATE(SUM(t.trans_amount), 2) AS total_amount, COUNT(t.trans_amount) AS total_transactions, t.trans_card_type
from batches b
JOIN transactions t on b.id = t.batch_id
GROUP BY batchId, t.trans_card_type;


-- display stats for a merchant and a given date range
select TRUNCATE(SUM(t.trans_amount), 2) AS total_amount, COUNT(t.id) AS total_transactions
from merchants m
join transactions t on m.mid = t.mid
where t.trans_date BETWEEN '2018-05-04' AND '2018-05-04'
GROUP BY m.mid;

-- display top 10 merchants (by total amount) for a given date range
--  *      merchant id, merchant name, total amount, number of transactions
select TRUNCATE(SUM(t.trans_amount), 2) AS total_amount, COUNT(t.id) AS total_transactions
from merchants m
join transactions t on m.mid = t.mid
GROUP BY m.mid
order by total_amount desc
limit 10;
