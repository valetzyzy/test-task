<?php

require_once 'ParseCsv.php';

$filePath = __DIR__ . '/report.csv';

$mapping = [
    ReportFields::TRANSACTION_DATE        => 'Transaction Date',
    ReportFields::TRANSACTION_TYPE        => 'Transaction Type',
    ReportFields::TRANSACTION_CARD_TYPE   => 'Transaction Card Type',
    ReportFields::TRANSACTION_CARD_NUMBER => 'Transaction Card Number',
    ReportFields::TRANSACTOIN_AMOUNT      => 'Transaction Amount',
    ReportFields::BATCH_DATE              => 'Batch Date',
    ReportFields::BATCH_REF_NUM           => 'Batch Reference Number',
    ReportFields::MERCHANT_ID             => 'Merchant ID',
    ReportFields::MERCHANT_NAME           => 'Merchant Name',
];

try {
    $parser = new ParseCsv($filePath, $mapping);

    $headers = [];
    $lines = [];
    foreach ($parser->getLines() as $n => $line) {
        if ($n === 0) {
            $headers = $parser->validateHeaders($line);
            if (isset($headers['errors'])) {
                ParseCsv::log(implode(PHP_EOL, $headers['errors']));
            }
            continue;
        }

        $line = str_getcsv($line);;
        $lines[] = ParseCsv::arrayReplaceKeys($line, $headers);
    }

    if (empty($lines)) ParseCsv::log('File is empty');

    $parser->saveToDb($lines, 1000);
} catch (Exception $e) {
    ParseCsv::log($e->getMessage());
}

ParseCsv::log('Done :)', 'Info');
exit;


